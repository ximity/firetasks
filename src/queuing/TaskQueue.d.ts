/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
import * as functions from 'firebase-functions';
import Event = functions.Event;
import DeltaSnapshot = functions.database.DeltaSnapshot;
import Task from '../core/Task';
import TaskHandlerDefinition from '../handlers/TaskHandlerDefinition';
export default class TaskQueue {
    static fromFirebaseEvent<T extends Task>(event: Event<DeltaSnapshot>, taskDefs: TaskHandlerDefinition): Promise<void>;
    private static handleError(event, taskJson, err);
    private static handleUnsupportedDomain(event, taskJson);
    private static handleUnsupportedAction(event, taskJson);
    private static handleMissingField(event, taskJson, field);
}
