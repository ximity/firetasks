/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
import { Optional } from 'scalts';
export default class TaskError implements Error {
    readonly code: number;
    readonly message: string;
    readonly devMessages: string[];
    readonly extendedCode: Optional<number>;
    name: string;
    constructor(code: number, message: string, devMessages?: string[], extendedCode?: Optional<number>);
    causesAsString(): string | null;
}
