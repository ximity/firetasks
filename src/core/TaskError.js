"use strict";
/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
Object.defineProperty(exports, "__esModule", { value: true });
const scalts_1 = require("scalts");
class TaskError {
    constructor(code, message, devMessages = [], extendedCode = scalts_1.None) {
        this.code = code;
        this.message = message;
        this.devMessages = devMessages;
        this.extendedCode = extendedCode;
        this.name = 'TaskError';
    }
    causesAsString() {
        if (this.devMessages.isEmpty)
            return null;
        else if (this.devMessages.length === 1)
            return this.devMessages[0];
        else
            return this.devMessages.map(s => `"${s}"`).join(',');
    }
}
exports.default = TaskError;
