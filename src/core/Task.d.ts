import * as admin from 'firebase-admin';
import Reference = admin.database.Reference;
import { Serializable } from 'ts-serialize';
import { Optional } from 'scalts';
import { JsObject } from 'ts-json-definition';
import TaskError from './TaskError';
export default abstract class Task extends Serializable {
    _domain: string;
    _action: string;
    _location: Optional<string>;
    _state: Optional<string>;
    _code: Optional<number>;
    _extendedCode: Optional<number>;
    _message: Optional<string>;
    _devMessage: Optional<string>;
    _ref: Reference;
    success(msg?: Optional<string>, devMsg?: Optional<string>, extCode?: Optional<number>): Task;
    successWithCode(code: number, msg?: Optional<string>, devMsg?: Optional<string>, extCode?: Optional<number>): Task;
    error(code: number, msg?: Optional<string>, devMsg?: Optional<string>, extCode?: Optional<number>): Task;
    abstract validate(): Optional<TaskError>;
    sanitize(remove?: string[], keep?: string[]): Object;
    toJson(): JsObject;
}
