"use strict";
/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const ts_serialize_1 = require("ts-serialize");
const scalts_1 = require("scalts");
const common_1 = require("../common");
class Task extends ts_serialize_1.Serializable {
    success(msg = scalts_1.None, devMsg = scalts_1.None, extCode = scalts_1.None) {
        return this.successWithCode(200, msg, devMsg, extCode);
    }
    successWithCode(code, msg = scalts_1.None, devMsg = scalts_1.None, extCode = scalts_1.None) {
        const newTask = _.cloneDeep(this);
        newTask._state = scalts_1.Some('success');
        newTask._code = scalts_1.Some(code);
        newTask._extendedCode = extCode;
        newTask._message = msg;
        newTask._devMessage = devMsg;
        return newTask;
    }
    error(code, msg = scalts_1.None, devMsg = scalts_1.None, extCode = scalts_1.None) {
        const newTask = _.cloneDeep(this);
        newTask._state = scalts_1.Some('error');
        newTask._code = scalts_1.Some(code);
        newTask._extendedCode = extCode;
        newTask._message = msg;
        newTask._devMessage = devMsg;
        return newTask;
    }
    sanitize(remove = [], keep = []) {
        return common_1.deleteProperties(this.toJson(), (o, k) => {
            return !keep.includes(k) && (k.startsWith('_') || remove.includes(k));
        });
    }
    toJson() {
        const obj = _.assign({ "_stateChanged": common_1.SERVER_VALUE }, super.toJson());
        return common_1.deleteProperties(obj, (o, k) => { return o[k] === null; });
    }
}
__decorate([
    ts_serialize_1.Serialize(),
    __metadata("design:type", String)
], Task.prototype, "_domain", void 0);
__decorate([
    ts_serialize_1.Serialize(),
    __metadata("design:type", String)
], Task.prototype, "_action", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(String),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_location", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(String),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_state", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(Number),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_code", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(Number),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_extendedCode", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(String),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_message", void 0);
__decorate([
    ts_serialize_1.SerializeOpt(String),
    __metadata("design:type", scalts_1.Optional)
], Task.prototype, "_devMessage", void 0);
exports.default = Task;
