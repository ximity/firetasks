/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
import { JsObject } from 'ts-json-definition';
export declare const SERVER_VALUE: {
    '.sv': string;
};
export declare function isDefined(value: any): boolean;
export declare function deleteProperties(obj: JsObject, pred: (JsObject, string) => boolean): JsObject;
export declare function computeHashCode(s: string): number;
