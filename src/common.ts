/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */

import { JsObject } from 'ts-json-definition';

export const SERVER_VALUE = { '.sv': 'timestamp' };

export function isDefined(value: any): boolean {
    return value !== null && value !== undefined;
}

export function deleteProperties(obj: JsObject, pred: (JsObject, string) => boolean): JsObject {
    const keys = Object.keys(obj);
    keys.forEach(key => {
        if (pred(obj, key)) delete obj[key];
    });
    return obj;
}

export function computeHashCode(s: string) {
    let hash = 0;
    if (!s || s.length === 0) return hash;
    for (let i = 0; i < s.length; i++) {
        const char = s.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
