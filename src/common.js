"use strict";
/**
 * Copyright (C) 2017 Ximity Inc. <http://www.ximity.net>
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERVER_VALUE = { '.sv': 'timestamp' };
function isDefined(value) {
    return value !== null && value !== undefined;
}
exports.isDefined = isDefined;
function deleteProperties(obj, pred) {
    const keys = Object.keys(obj);
    keys.forEach(key => {
        if (pred(obj, key))
            delete obj[key];
    });
    return obj;
}
exports.deleteProperties = deleteProperties;
function computeHashCode(s) {
    let hash = 0;
    if (!s || s.length === 0)
        return hash;
    for (let i = 0; i < s.length; i++) {
        const char = s.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
exports.computeHashCode = computeHashCode;
